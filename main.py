#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 12:21:44 2022

@author: elie
"""

import sys
from pbmodele import *
from sat_3sat import *
from satsolveur import *
from grille import *


if __name__ == '__main__':
    ECRIRE_SORTIE = False
    # Ecrire dans des fichiers
    if sys.argv.count("-s") != 0:
        ECRIRE_SORTIE = True
    
    
    
    # Initialisation de la grille initiale depuis le fichier donné en argument
    g = Grille()
    
    if len(sys.argv) >= 1:
        g.construireGrilleFichier(sys.argv[1])
        doc_name = sys.argv[1]
    else:
        g.construireGrilleClavier()
        if ECRIRE_SORTIE:
            doc_name = input("Entrer le nom des fichiers (sans extension) : ")
            print("\n--------------------------------\n")
    
    print("Grille de jeu initiale :")
    g.ecrireGrille()
    
    # on determine la base des fichiers sorties généres plus tard
    if ECRIRE_SORTIE:
        doc_name = doc_name.replace(".tkz", "")
    
    
    ##############################
    #                            #
    #    PARTIE FORMALISATION    #
    #         DU PROBLEME        #
    #                            #
    ##############################
    print("\nFormalisation du problème       --> ", doc_name, ".dmcs", sep="")
    
    # on instancie le problème
    pb = ProblemeModele(g)
    pb.determinerClauses()
    
    # Vérifiaction du nombre de clauses obtenu
    nb_reel = len(pb.clauses)
    nb_theo = pb.nombreClauses()
    print("Nombre théorique de clauses : ", nb_theo)
    print("Nombre obtenu de clauses    : ", nb_reel)
    
    # Erreur : pas le bon nombre de clauses
    if nb_theo != nb_reel:
        print("Erreur lors de la formalisation :" +
              " pas le nombre attendu de clauses")
        sys.exit(2)
    
    # Ecrit l'ensemble des clauses dans un fichier dimacs
    if ECRIRE_SORTIE:
        pb.sortieDimacs(doc_name + ".dmcs")
    
    
    ##############################
    #                            #
    #     PARTIE CONVERSION      #
    #      DE SAT VERS 3-SAT     #
    #                            #
    ##############################
    print("\nClauses SAT --> Clauses 3-SAT   --> ", doc_name, "-3.dmcs", sep="")
    
    # creation de l'instance de conversion
    pb3sat = Sat3SAT(pb.clauses, pb.nb_variables)
    
    # on effectue le passage
    pb3sat.conversion()
    
    if ECRIRE_SORTIE:
        pb3sat.sortieDimacs(doc_name + "-3.dmcs")
    
    
    ##############################
    #                            #
    #     PARTIE RESOLUTION      #
    #        SAT SOLVEUR         #
    #                            #
    ##############################
    print("\nResolution du problème          --> ", doc_name, ".sol", sep="")
    
    solveur = SatSolveur(pb.clauses, pb.nb_variables)
    solveur.lancerSolveur()
    
    l_grille = []
    
    for i in range(0, solveur.nb_variables):
        if solveur.assignation[i] == 1:
            l_grille.append(i+1)
        else:
            l_grille.append(-i-1)
    
    gs = Grille(g.taille, l_grille)
    gs.ecrireGrille()

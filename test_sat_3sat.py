#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 21:24:04 2022

@author: elie
"""

from sat_3sat import *

sat3 = Sat3SAT([
        [1],
        [-3, 2],
        [-4,6,-5],
        [7,8,9,15,-11,-12,13,14,10],
        [12,-5],
        [-4,5,4]
    ], 15)

sat3.conversion()

print(sat3.clauses)
print("-------------------------------------------")
print(sat3.clauses_simplifiees)
print("-------------------------------------------")
print(sat3.nb_variables)

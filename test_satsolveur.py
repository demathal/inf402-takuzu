#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 11:50:11 2022

@author: elie
"""

from satsolveur import *

solv = SatSolveur([
        [1, 2],
        [-5, 5],
        [-4, 3, -5],
        [1, -2, 4],
        [-3, 1, -4],
        [1,2, -3]
    ], 5)

print("-------- CLAUSES -----------------------")
print(solv.clauses)

print("-------- ASSIGNATION ALEATOIRE ---------")
solv.assignation = [0, 0, 1, 1, 0]
print(solv.assignation)

print("-------- TEST SATISFIABILITE -----------")

solv.trouveClausesInsatisfiable()
print(solv.clauses_insatisfaites)
print(solv.estModele())

print("-------- CLAUSE ALEATOIRE --------------")
i = solv.choixClauseInsatisfaiteAleatoire()
print(i)

print("-------- VARIABLE ALEATOIRE ------------")
i = solv.choixVariableAleatoire(4)
print(i)

print("-------- VARIABLE DETERMINISTE ------------")
i = solv.choixVariableDeterministe(4)
print(i)

print("-------- Test solveur ------------")
res = solv.lancerSolveur()
print(res)
print(solv.assignation)
# Projet d'INF402 - Takuzu

Projet d'INF402 - resolution d'une grille de Takuzu avec un SAT-solveur. Python.

## Rapport de projet


Le rapport sur lequel se base ce programme est disponible à l'adresse suivante :

[https://fr.overleaf.com/read/pkyjqgcshzjd](https://fr.overleaf.com/read/pkyjqgcshzjd)


## Execution du programme

### Avertissement

Le programme n'a pas été conçu pour gérer les erreurs de saisies.
Si l'utilisateur entre n'importe quoi le non bon fonctionnement du programme
est de sa responsabilité.

### Principe d'utilisation principale

Vous avez deux possibilités pour résoudre une grille de Takuzu :
1. Ecrire les données du problème dans un fichier (.tkz) respectant la structure suivante

```
largeur_de_la_grille
nombre_zeros (nz)
nombres_uns  (nu)
suivit de nz+nu coordonnées de la forme i,j
...
```

2. Au clavier sur le même principe que ci-dessus.

Pour éxécuter il faut ensuite entrer :
```
python3 main.py [fichier.tkz] [-s]
```

L'option `-s` permet d'écrire les resultat de chaque operation dans des fichiers.

### Solveur

Pour le solveur par exemple :

```
minisat fichier_source.dcms fichier_sortie.dcms
```

Vous pouvez ensuite le lire et l'interpréter à l'aide de :
```
cat fichier_sortie.dcms
```
Les cases sont numérotés de 1 à n^2 (avec n la largeur de la grille).
Si c'est possitif alors c'est un 1 si c'est négatif c'est un 0.


## Organisation des fichiers

### Modules principaux

Il y  a dans ce projet 6 modules principaux :
1. La classe `Grille` qui définit une grille de jeu plus ou moins remplies
2. La librairie `es_dimacs` qui contient les fonctions d'entrée/sortie dans des fichiers dimacs.
2. Le module `pbmodele` permet à partir d'une grille de modéliser un problème sous forme de clauses qu'on peut écrire dans un fichier dimacs.
3. Le module `sat_3sat` qui permet de convertir un ensemble de clauses en ensemble de clauses à trois littéraux.
4. Le module `satsolveur` qui permet de trouver un modele d'un ensemble de 3-clauses.
5. Un programme principal qui permet de trouver la solution (si possible) d'un problème saisi par l'utilisateur (cf ci-dessous).

### Modules de test

Ce projet comporte 3 modules de tests : trois programmes qui testent séparement chaque fonction des modules `pbmodele`, `sat_3sat` et `satsolveur`.

## Utilisation de git

### Cloner un dépot

```
cd repertoire_existant
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/carrote/inf402-takuzu.git
```

### Voir les différences entre le dépot et le repertoire cloné

```
git diff [fichier]
```

### Faire monter des modifications sur le dépot

```
git add fichiers_modifies_a_ajouter
git rm [-r] fichiers_a_enlever
git commit -m "Commentaires sur les modifications"
git push
```

### Faire descendre des modifications du le dépot

```
git pull
```

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 12:10:29 2022

@author: qui voudra ??
"""

import sys
from pbmodele import *
from grille import *
import es_dimacs


if __name__ == '__main__':
    g = Grille()
    g.construireGrilleFichier("tests/grille_ex_4.tkz")
    
    print("Grille de jeu initiale :")
    g.ecrireGrille()
    
    pb = ProblemeModele(g)
    pb.determinerClauses()
    pb.sortieDimacs("toto.dmcs")
    
    u = es_dimacs.lireCNFDimacs("toto.dmcs")
    print(u[1][7])
    
    
    #####################################
    #                                   #
    #                                   #
    #            A FAIRE !!!            #
    #                                   #
    #                                   #
    #####################################



